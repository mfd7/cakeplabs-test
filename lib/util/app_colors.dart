import 'dart:ui';

class AppColors {
  static const white = Color(0xFFFFFFFF);
  static const black = Color(0xFF000000);
  static const smokeyGrey = Color(0xFF707070);
  static const valentineRed = Color(0xFFEE4958);
  static const osloGrey = Color(0xFF8B8B8B);
  static const ashGrey = Color(0xFFBBBBBB);
  static const dawnPink = Color(0xFFEBEBEB);
}
