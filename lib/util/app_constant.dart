class AppConstant {
  static const String kToken =
      'eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiIzM2EwYmNiZDFjNDUxYzFlNGI0ZWUyZTRjYmIzNTdmNSIsInN1YiI6IjYxNGFhYjFjMjJkZjJlMDAyYzlhN2IzMCIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.qgIhaGUJffF1DbrOaYJ9aCE9O5d-NrPozVvNdrNtvvs';
  static const int timeoutSeconds = 5 * 60;
  static const kDefaultPadding = 16.0;
}
