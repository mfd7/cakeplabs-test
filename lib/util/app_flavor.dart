enum Flavor { dev, prod, sit }

class AppFlavor {
  final Flavor flavor;
  final String baseUrl;
  final String imgUrl;

  static late AppFlavor _instance;
  static bool initialize = false;

  factory AppFlavor({
    required flavor,
    required baseUrl,
    required imgUrl,
  }) {
    initialize = true;
    _instance = AppFlavor._initialize(flavor, baseUrl, imgUrl);
    return _instance;
  }

  AppFlavor._initialize(this.flavor, this.baseUrl, this.imgUrl);

  static AppFlavor get instance {
    return _instance;
  }

  static bool get isDev => instance.flavor == Flavor.dev;
  static bool get isProd => instance.flavor == Flavor.prod;
}
