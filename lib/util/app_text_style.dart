import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class AppTextStyle {
  static final mulishLargeRegular = GoogleFonts.mulish(
    textStyle: const TextStyle(
      fontWeight: FontWeight.normal,
      fontSize: 20.0,
    ),
  );

  static final mulishLargeBold = GoogleFonts.mulish(
    textStyle: const TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 16.0,
    ),
  );

  static final mulishMedium = GoogleFonts.mulish(
    textStyle: const TextStyle(
      fontWeight: FontWeight.bold,
      fontSize: 14.0,
    ),
  );

  static final mulishSmall = GoogleFonts.mulish(
    textStyle: const TextStyle(
      fontWeight: FontWeight.normal,
      fontSize: 12.0,
    ),
  );

  static final mulishXSmall = GoogleFonts.mulish(
    textStyle: const TextStyle(
      fontWeight: FontWeight.normal,
      fontSize: 10.0,
    ),
  );
}
