import 'package:cakeplabs_test/features/home/bloc/genre_list/genre_list_bloc.dart';
import 'package:cakeplabs_test/features/home/bloc/movie_by_genre/movie_by_genre_bloc.dart';
import 'package:cakeplabs_test/features/home/bloc/now_playing/now_playing_bloc.dart';
import 'package:cakeplabs_test/injection.dart' as di;
import 'package:cakeplabs_test/util/app_router.dart';
import 'package:cakeplabs_test/util/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_alice/alice.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:overlay_support/overlay_support.dart';

class CakeplabsApp extends StatefulWidget {
  const CakeplabsApp({super.key});

  @override
  State<CakeplabsApp> createState() => _CakeplabsAppState();
}

class _CakeplabsAppState extends State<CakeplabsApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (_) => di.locator<NowPlayingBloc>()),
        BlocProvider(create: (_) => di.locator<GenreListBloc>()),
        BlocProvider(create: (_) => di.locator<MovieByGenreBloc>()),
      ],
      child: OverlaySupport.global(
        child: MaterialApp(
          navigatorKey: di.locator<Alice>().getNavigatorKey(),
          title: 'CakepLabs',
          onGenerateRoute: AppRouter().generateRoute,
          initialRoute: Routes.homeRoute,
        ),
      ),
    );
  }
}
