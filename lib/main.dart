import 'package:cakeplabs_test/cakeplabs_app.dart';
import 'package:cakeplabs_test/util/app_flavor.dart';
import 'package:cakeplabs_test/injection.dart' as di;
import 'package:flutter/material.dart';

main() async {
  WidgetsFlutterBinding.ensureInitialized();
  di.init();
  AppFlavor(
    baseUrl: 'https://api.themoviedb.org/3',
    imgUrl: 'https://image.tmdb.org/t/p/original',
    flavor: Flavor.prod,
  );
  runApp(const CakeplabsApp());
}
