import 'package:cakeplabs_test/core/data/failure.dart';
import 'package:cakeplabs_test/core/domains/genre/entities/genre.dart';
import 'package:cakeplabs_test/core/domains/genre/repositories/genre_repository.dart';
import 'package:dartz/dartz.dart';

class RetrieveGenreListUsecase {
  final GenreRepository genreRepository;

  RetrieveGenreListUsecase(this.genreRepository);

  Future<Either<Failure, List<Genre>>> execute() async {
    return genreRepository.retrieveGenreList();
  }
}
