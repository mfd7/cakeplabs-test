import 'package:cakeplabs_test/core/data/failure.dart';
import 'package:cakeplabs_test/core/domains/genre/entities/genre.dart';
import 'package:dartz/dartz.dart';

abstract class GenreRepository {
  Future<Either<Failure, List<Genre>>> retrieveGenreList();
}
