import 'package:cakeplabs_test/core/data/failure.dart';
import 'package:cakeplabs_test/core/domains/movie/entities/movie_by_genre.dart';
import 'package:cakeplabs_test/core/domains/movie/repositories/movie_repository.dart';
import 'package:dartz/dartz.dart';

class RetrieveMovieByGenreUsecase {
  final MovieRepository movieRepository;

  RetrieveMovieByGenreUsecase(this.movieRepository);

  Future<Either<Failure, MovieByGenre>> execute(
      {int? genreId, required int page}) {
    return movieRepository.retrieveMovieByGenre(genreId, page);
  }
}
