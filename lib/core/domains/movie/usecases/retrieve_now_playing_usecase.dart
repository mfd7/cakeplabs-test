import 'package:cakeplabs_test/core/data/failure.dart';
import 'package:cakeplabs_test/core/domains/movie/entities/movie.dart';
import 'package:cakeplabs_test/core/domains/movie/repositories/movie_repository.dart';
import 'package:dartz/dartz.dart';

class RetrieveNowPlayingUsecase {
  final MovieRepository repository;

  RetrieveNowPlayingUsecase(this.repository);

  Future<Either<Failure, List<Movie>>> execute() {
    return repository.retrieveNowPlayingList();
  }
}
