import 'package:cakeplabs_test/core/data/failure.dart';
import 'package:cakeplabs_test/core/domains/movie/entities/movie.dart';
import 'package:cakeplabs_test/core/domains/movie/entities/movie_by_genre.dart';
import 'package:dartz/dartz.dart';

abstract class MovieRepository {
  Future<Either<Failure, List<Movie>>> retrieveNowPlayingList();
  Future<Either<Failure, MovieByGenre>> retrieveMovieByGenre(
      int? genreId, int page);
}
