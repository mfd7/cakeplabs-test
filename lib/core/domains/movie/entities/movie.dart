class Movie {
  final String title;
  final String imageUrl;
  final String backdropUrl;
  final DateTime? releaseDate;

  Movie(
      {this.title = '',
      this.imageUrl = '',
      this.backdropUrl = '',
      this.releaseDate});
}
