import 'package:cakeplabs_test/core/domains/genre/usecases/retrieve_genre_list_usecase.dart';
import 'package:cakeplabs_test/core/domains/movie/usecases/retrieve_movie_by_genre_usecase.dart';
import 'package:cakeplabs_test/core/domains/movie/usecases/retrieve_now_playing_usecase.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerLazySingleton(() => RetrieveNowPlayingUsecase(locator()));
  locator.registerLazySingleton(() => RetrieveGenreListUsecase(locator()));
  locator.registerLazySingleton(() => RetrieveMovieByGenreUsecase(locator()));
}
