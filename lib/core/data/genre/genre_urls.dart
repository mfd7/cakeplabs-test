import 'package:cakeplabs_test/core/data/urls.dart';

class GenreUrls {
  static String genreList = '${Urls.baseUrl}/genre/movie/list';
}
