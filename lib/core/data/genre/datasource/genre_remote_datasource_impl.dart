import 'package:cakeplabs_test/core/data/api_base_helper.dart';
import 'package:cakeplabs_test/core/data/exception.dart';
import 'package:cakeplabs_test/core/data/genre/datasource/genre_remote_datasource.dart';
import 'package:cakeplabs_test/core/data/genre/genre_urls.dart';
import 'package:cakeplabs_test/core/data/genre/models/genre_model.dart';

class GenreRemoteDatasourceImpl implements GenreRemoteDatasource {
  final ApiBaseHelper apiBaseHelper;

  GenreRemoteDatasourceImpl(this.apiBaseHelper);

  @override
  Future<List<GenreModel>> retrieveGenreList() async {
    final response = await apiBaseHelper.getApi(
      GenreUrls.genreList,
    );
    try {
      List<dynamic> parsedListJson = response['genres'];
      return List<GenreModel>.from(
        parsedListJson.map<GenreModel>(
          (i) => GenreModel.fromJson(i),
        ),
      );
    } catch (_) {
      throw ParseDataException();
    }
  }
}
