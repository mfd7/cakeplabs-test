import 'package:cakeplabs_test/core/data/genre/models/genre_model.dart';

abstract class GenreRemoteDatasource {
  Future<List<GenreModel>> retrieveGenreList();
}
