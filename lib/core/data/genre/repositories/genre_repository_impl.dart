import 'package:cakeplabs_test/core/data/exception.dart';
import 'package:cakeplabs_test/core/data/failure.dart';
import 'package:cakeplabs_test/core/data/genre/datasource/genre_remote_datasource.dart';
import 'package:cakeplabs_test/core/domains/genre/entities/genre.dart';
import 'package:cakeplabs_test/core/domains/genre/repositories/genre_repository.dart';
import 'package:dartz/dartz.dart';

class GenreRepositoryImpl implements GenreRepository{
  final GenreRemoteDatasource remoteDatasource;

  GenreRepositoryImpl(this.remoteDatasource);

  @override
  Future<Either<Failure, List<Genre>>> retrieveGenreList() async {
    try {
      final responses = await remoteDatasource.retrieveGenreList();
      return Right(responses.map((e) => e.toEntity()).toList());
    } on BadRequestException catch (e) {
      return Left(RequestFailure(e.toString()));
    } on ServerErrorException {
      return const Left(ServerFailure());
    } on ParseDataException {
      return const Left(NoDataFailure());
    } on NoConnectionException {
      return const Left(ConnectionFailure());
    } on UnauthorisedException {
      return const Left(UnAuthorizedFailure());
    }
  }

}