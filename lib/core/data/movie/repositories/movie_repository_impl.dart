import 'package:cakeplabs_test/core/data/exception.dart';
import 'package:cakeplabs_test/core/data/failure.dart';
import 'package:cakeplabs_test/core/data/movie/datasources/movie_remote_datasource.dart';
import 'package:cakeplabs_test/core/domains/movie/entities/movie.dart';
import 'package:cakeplabs_test/core/domains/movie/entities/movie_by_genre.dart';
import 'package:cakeplabs_test/core/domains/movie/repositories/movie_repository.dart';
import 'package:dartz/dartz.dart';

class MovieRepositoryImpl implements MovieRepository {
  final MovieRemoteDatasource remoteDatasource;

  MovieRepositoryImpl(this.remoteDatasource);

  @override
  Future<Either<Failure, List<Movie>>> retrieveNowPlayingList() async {
    try {
      final responses = await remoteDatasource.retrieveNowPlayingList();
      return Right(responses.map((e) => e.toEntity()).toList());
    } on BadRequestException catch (e) {
      return Left(RequestFailure(e.toString()));
    } on ServerErrorException {
      return const Left(ServerFailure());
    } on ParseDataException {
      return const Left(NoDataFailure());
    } on NoConnectionException {
      return const Left(ConnectionFailure());
    } on UnauthorisedException {
      return const Left(UnAuthorizedFailure());
    }
  }

  @override
  Future<Either<Failure, MovieByGenre>> retrieveMovieByGenre(
      int? genreId, int page) async {
    try {
      final responses =
          await remoteDatasource.retrieveMovieByGenre(genreId, page);
      return Right(responses.toEntity());
    } on BadRequestException catch (e) {
      return Left(RequestFailure(e.toString()));
    } on ServerErrorException {
      return const Left(ServerFailure());
    } on ParseDataException {
      return const Left(NoDataFailure());
    } on NoConnectionException {
      return const Left(ConnectionFailure());
    } on UnauthorisedException {
      return const Left(UnAuthorizedFailure());
    }
  }
}
