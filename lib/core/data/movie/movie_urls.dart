import 'package:cakeplabs_test/core/data/urls.dart';

class MovieUrls {
  static String nowPlaying = '${Urls.baseUrl}/movie/now_playing';
  static String movieByGenre = '${Urls.baseUrl}/discover/movie';
}
