import 'package:cakeplabs_test/core/data/movie/models/movie_by_genre_model.dart';
import 'package:cakeplabs_test/core/data/movie/models/movie_model.dart';

abstract class MovieRemoteDatasource {
  Future<List<MovieModel>> retrieveNowPlayingList();
  Future<MovieByGenreModel> retrieveMovieByGenre(int? genreId, int page);
}
