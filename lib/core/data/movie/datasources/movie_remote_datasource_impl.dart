import 'package:cakeplabs_test/core/data/api_base_helper.dart';
import 'package:cakeplabs_test/core/data/exception.dart';
import 'package:cakeplabs_test/core/data/movie/datasources/movie_remote_datasource.dart';
import 'package:cakeplabs_test/core/data/movie/models/movie_by_genre_model.dart';
import 'package:cakeplabs_test/core/data/movie/models/movie_model.dart';
import 'package:cakeplabs_test/core/data/movie/movie_urls.dart';
import 'package:cakeplabs_test/util/log_util.dart';

class MovieRemoteDatasourceImpl implements MovieRemoteDatasource {
  final ApiBaseHelper apiBaseHelper;

  MovieRemoteDatasourceImpl(this.apiBaseHelper);

  @override
  Future<List<MovieModel>> retrieveNowPlayingList() async {
    final response = await apiBaseHelper.getApi(
      MovieUrls.nowPlaying,
    );
    try {
      List<dynamic> parsedListJson = response['results'];
      return List<MovieModel>.from(
        parsedListJson.map<MovieModel>(
          (i) => MovieModel.fromJson(i),
        ),
      );
    } catch (_) {
      throw ParseDataException();
    }
  }

  @override
  Future<MovieByGenreModel> retrieveMovieByGenre(int? genreId, int page) async {
    final response =
        await apiBaseHelper.getApi(MovieUrls.movieByGenre, qParams: {
      'with_genres': genreId != null ? genreId.toString() : '',
      'page': page.toString()
    });
    try {
      return MovieByGenreModel.fromJson(response);
    } catch (_) {
      throw ParseDataException();
    }
  }
}
