class AppException implements Exception {
  final String? _message;
  final String? _prefix;
  final bool isPinError;
  final bool isCardError;
  final bool isDeviceError;

  AppException([
    this._message,
    this._prefix,
    this.isPinError = false,
    this.isCardError = false,
    this.isDeviceError = false,
  ]);

  @override
  String toString() {
    return "$_prefix$_message";
  }
}

class FetchDataException extends AppException {
  FetchDataException([String? message])
      : super(message, "Kesalahan selama komunikasi: ");
}

class BadRequestException extends AppException {
  BadRequestException([
    message,
    String? prefix,
    bool isPinError = false,
    bool isCardError = false,
    bool isDeviceError = false,
  ]) : super(
    message,
    prefix ?? '',
    isPinError,
    isCardError,
    isDeviceError,
  );
}

class UnauthorisedException extends AppException {
  UnauthorisedException([message]) : super(message, "Tidak terautentikasi: ");
}

class ServerErrorException extends AppException {
  ServerErrorException([String? message])
      : super(message, "Server mengembalikan galat: ");
}

class ParseDataException extends AppException {
  ParseDataException([message])
      : super(message, "Gagal mengurai data dari respons: ");
}

class NoConnectionException extends AppException {
  NoConnectionException() : super("Tidak ada koneksi internet.");
}

class DataNotFoundException extends AppException {
  DataNotFoundException() : super("Tidak ada data ditemukan.");
}

class FormatException extends AppException {
  FormatException() : super("Galat mengurai respons format dari server.");
}

class LocationServiceException extends AppException {
  LocationServiceException() : super("Gagal untuk mengaktifkan gps.");
}

class LocationPermissionException extends AppException {
  LocationPermissionException() : super("Gagal mendapatkan izin lokasi.");
}
