import 'package:equatable/equatable.dart';

abstract class Failure extends Equatable {
  final String message;

  const Failure(this.message);

  @override
  List<Object> get props => [message];
}

class DeviceFailure extends Failure {
  const DeviceFailure(String message) : super(message);
}

class RequestFailure extends Failure {
  const RequestFailure([
    String message = 'error_request',
  ]) : super(message);
}

class ServerFailure extends Failure {
  const ServerFailure([
    String message = 'error_server',
  ]) : super(message);
}

class ConnectionFailure extends Failure {
  const ConnectionFailure([
    String message = 'error_connection',
  ]) : super(message);
}

class NoDataFailure extends Failure {
  const NoDataFailure([
    String message = 'error_no_data',
  ]) : super(message);
}

class DatabaseFailure extends Failure {
  const DatabaseFailure([
    String message = 'error_database',
  ]) : super(message);
}

class OtherFailure extends Failure {
  const OtherFailure(String message) : super(message);
}

class UnAuthorizedFailure extends Failure {
  const UnAuthorizedFailure([
    String message = 'error_unauthorized',
  ]) : super(message);
}

class LocationServiceFailure extends Failure {
  const LocationServiceFailure([
    String message = 'error_activate_gps',
  ]) : super(message);
}

class LocationPermissionFailure extends Failure {
  const LocationPermissionFailure([
    String message = 'error_gps_permission',
  ]) : super(message);
}
