import 'package:cakeplabs_test/core/data/genre/datasource/genre_remote_datasource.dart';
import 'package:cakeplabs_test/core/data/genre/datasource/genre_remote_datasource_impl.dart';
import 'package:cakeplabs_test/core/data/movie/datasources/movie_remote_datasource.dart';
import 'package:cakeplabs_test/core/data/movie/datasources/movie_remote_datasource_impl.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerLazySingleton<MovieRemoteDatasource>(
      () => MovieRemoteDatasourceImpl(locator()));
  locator.registerLazySingleton<GenreRemoteDatasource>(
      () => GenreRemoteDatasourceImpl(locator()));
}
