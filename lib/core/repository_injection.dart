import 'package:cakeplabs_test/core/data/genre/repositories/genre_repository_impl.dart';
import 'package:cakeplabs_test/core/data/movie/repositories/movie_repository_impl.dart';
import 'package:cakeplabs_test/core/domains/genre/repositories/genre_repository.dart';
import 'package:cakeplabs_test/core/domains/movie/repositories/movie_repository.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerLazySingleton<MovieRepository>(
      () => MovieRepositoryImpl(locator()));
  locator.registerLazySingleton<GenreRepository>(
      () => GenreRepositoryImpl(locator()));
}
