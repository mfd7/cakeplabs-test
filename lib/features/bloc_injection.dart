import 'package:cakeplabs_test/features/home/bloc/genre_list/genre_list_bloc.dart';
import 'package:cakeplabs_test/features/home/bloc/movie_by_genre/movie_by_genre_bloc.dart';
import 'package:cakeplabs_test/features/home/bloc/now_playing/now_playing_bloc.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  locator.registerFactory(() => NowPlayingBloc(locator()));
  locator.registerFactory(() => GenreListBloc(locator()));
  locator.registerFactory(() => MovieByGenreBloc(locator()));
}
