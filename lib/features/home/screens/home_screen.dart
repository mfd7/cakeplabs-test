import 'package:cakeplabs_test/features/home/bloc/genre_list/genre_list_bloc.dart';
import 'package:cakeplabs_test/features/home/bloc/movie_by_genre/movie_by_genre_bloc.dart';
import 'package:cakeplabs_test/features/home/bloc/now_playing/now_playing_bloc.dart';
import 'package:cakeplabs_test/features/home/widgets/genre_chip.dart';
import 'package:cakeplabs_test/features/home/widgets/movies_by_genre.dart';
import 'package:cakeplabs_test/features/home/widgets/now_playing.dart';
import 'package:cakeplabs_test/features/widgets/custom_shimmer.dart';
import 'package:cakeplabs_test/util/app_colors.dart';
import 'package:cakeplabs_test/util/app_constant.dart';
import 'package:cakeplabs_test/util/app_text_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({super.key});

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final ValueNotifier<int> _selectedGenre = ValueNotifier<int>(0);
  final _scrollController = ScrollController();

  @override
  void initState() {
    super.initState();
    fetchAllData();
    _scrollController.addListener(_onScroll);
  }

  @override
  void dispose() {
    _scrollController.removeListener(_onScroll);
    super.dispose();
  }

  fetchAllData() {
    context.read<NowPlayingBloc>().add(OnRetrieveNowPlayingList());
    context.read<GenreListBloc>().add(OnRetrieveGenreList());
  }

  void _onScroll() {
    if (_isBottom) {
      context.read<MovieByGenreBloc>().add(OnRetrieveMovieByGenre(
          nextPage: true,
          genreId: (context.read<GenreListBloc>().state as GenreListLoaded)
              .genres[_selectedGenre.value]
              .id));
    }
  }

  bool get _isBottom {
    return _scrollController.position.pixels ==
        _scrollController.position.maxScrollExtent;
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener<GenreListBloc, GenreListState>(
      listener: (context, state) {
        if (state is GenreListLoaded) {
          context
              .read<MovieByGenreBloc>()
              .add(OnRetrieveMovieByGenre(genreId: state.genres.first.id));
        }
      },
      child: Scaffold(
        backgroundColor: AppColors.white,
        appBar: AppBar(
          title: Text(
            'The Movie List',
            style: AppTextStyle.mulishLargeRegular
                .copyWith(color: AppColors.black),
          ),
          centerTitle: true,
          backgroundColor: AppColors.white,
          elevation: 1,
        ),
        body: RefreshIndicator(
          onRefresh: () {
            fetchAllData();
            return Future(() => true);
          },
          child: SingleChildScrollView(
            controller: _scrollController,
            padding: const EdgeInsets.symmetric(
                // horizontal: AppConstant.kDefaultPadding,
                vertical: AppConstant.kDefaultPadding / 4 * 5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                BlocBuilder<NowPlayingBloc, NowPlayingState>(
                    builder: (context, state) {
                  if (state is NowPlayingLoading) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: AppConstant.kDefaultPadding),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomShimmer(
                              height: 20,
                              width: MediaQuery.of(context).size.width / 2),
                          const SizedBox(
                            height: AppConstant.kDefaultPadding,
                          ),
                          const CustomShimmer(
                              height: 130, width: double.infinity),
                        ],
                      ),
                    );
                  }
                  if (state is NowPlayingLoaded) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: AppConstant.kDefaultPadding),
                          child: Text(
                            'Now Playing Movies',
                            style: AppTextStyle.mulishLargeBold
                                .copyWith(color: AppColors.black),
                          ),
                        ),
                        const SizedBox(
                          height: AppConstant.kDefaultPadding,
                        ),
                        NowPlaying(
                          movies: state.movies,
                        ),
                      ],
                    );
                  }
                  if (state is NowPlayingError) {
                    return Center(
                      child: Text(state.message),
                    );
                  }
                  return Container();
                }),
                const SizedBox(
                  height: AppConstant.kDefaultPadding,
                ),
                BlocBuilder<GenreListBloc, GenreListState>(
                    builder: (context, state) {
                  if (state is GenreListLoading) {
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: AppConstant.kDefaultPadding),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          CustomShimmer(
                            height: 20,
                            width: MediaQuery.of(context).size.width / 2,
                          ),
                          const SizedBox(
                            height: AppConstant.kDefaultPadding,
                          ),
                          const CustomShimmer(
                            height: 70,
                            width: double.infinity,
                          ),
                        ],
                      ),
                    );
                  }
                  if (state is GenreListLoaded) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: AppConstant.kDefaultPadding),
                          child: Text(
                            'Genre List',
                            style: AppTextStyle.mulishLargeBold
                                .copyWith(color: AppColors.black),
                          ),
                        ),
                        const SizedBox(
                          height: AppConstant.kDefaultPadding,
                        ),
                        SizedBox(
                          height: 70,
                          child: ListView.separated(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: AppConstant.kDefaultPadding),
                              scrollDirection: Axis.horizontal,
                              physics: const BouncingScrollPhysics(),
                              itemBuilder: (context, index) {
                                return ValueListenableBuilder<int>(
                                  builder: (context, value, child) {
                                    return GenreChip(
                                      title: state.genres[index].name,
                                      isSelected: _selectedGenre.value == index,
                                      onTap: () {
                                        _selectedGenre.value = index;
                                        context.read<MovieByGenreBloc>().add(
                                            OnRetrieveMovieByGenre(
                                                genreId:
                                                    state.genres[index].id));
                                      },
                                    );
                                  },
                                  valueListenable: _selectedGenre,
                                );
                              },
                              separatorBuilder: (_, __) {
                                return const SizedBox(
                                  width: AppConstant.kDefaultPadding,
                                );
                              },
                              itemCount: state.genres.length),
                        ),
                      ],
                    );
                  }
                  if (state is GenreListError) {
                    return Center(
                      child: Text(
                        state.message,
                        style: AppTextStyle.mulishXSmall
                            .copyWith(color: AppColors.black),
                      ),
                    );
                  }
                  return Container();
                }),
                const SizedBox(
                  height: AppConstant.kDefaultPadding / 2 * 3,
                ),
                BlocBuilder<MovieByGenreBloc, MovieByGenreState>(
                    builder: (context, state) {
                  if (state is MovieByGenreLoading) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: AppConstant.kDefaultPadding),
                          child: CustomShimmer(
                            height: 20,
                            width: MediaQuery.of(context).size.width / 2,
                          ),
                        ),
                        const SizedBox(
                          height: AppConstant.kDefaultPadding,
                        ),
                        GridView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 1 / 1.35,
                            crossAxisSpacing: AppConstant.kDefaultPadding,
                            mainAxisSpacing: AppConstant.kDefaultPadding,
                          ),
                          padding: const EdgeInsets.symmetric(
                              horizontal: AppConstant.kDefaultPadding),
                          itemBuilder: (context, index) {
                            return Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                const CustomShimmer(
                                  height: 150,
                                  width: double.infinity,
                                ),
                                const SizedBox(
                                  height: AppConstant.kDefaultPadding,
                                ),
                                CustomShimmer(
                                  height: 10,
                                  width: MediaQuery.of(context).size.width / 4,
                                ),
                                const SizedBox(
                                  height: AppConstant.kDefaultPadding,
                                ),
                                CustomShimmer(
                                  height: 10,
                                  width: MediaQuery.of(context).size.width / 4,
                                ),
                              ],
                            );
                          },
                          itemCount: 10,
                        ),
                        const Padding(
                            padding: EdgeInsets.symmetric(
                                horizontal: AppConstant.kDefaultPadding),
                            child: CustomShimmer(
                              height: 150,
                              width: double.infinity,
                            )),
                      ],
                    );
                  }
                  if (state is MovieByGenreLoaded) {
                    return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: AppConstant.kDefaultPadding),
                          child: Text(
                            'Movies By Genre',
                            style: AppTextStyle.mulishLargeBold
                                .copyWith(color: AppColors.black),
                          ),
                        ),
                        const SizedBox(
                          height: AppConstant.kDefaultPadding / 4 * 5,
                        ),
                        GridView.builder(
                          shrinkWrap: true,
                          physics: const NeverScrollableScrollPhysics(),
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 2,
                            childAspectRatio: 1 / 1.35,
                            crossAxisSpacing: AppConstant.kDefaultPadding,
                            mainAxisSpacing: AppConstant.kDefaultPadding,
                          ),
                          padding: const EdgeInsets.symmetric(
                              horizontal: AppConstant.kDefaultPadding),
                          itemBuilder: (context, index) {
                            final item = state.movies.movies.elementAt(index);
                            return MoviesByGenre(
                              title: item.title,
                              imageUrl: item.imageUrl,
                              releaseDate: item.releaseDate ?? DateTime.now(),
                            );
                          },
                          itemCount: state.movies.movies.length,
                        ),
                        const SizedBox(
                          height: AppConstant.kDefaultPadding,
                        ),
                        const Center(
                          child: CircularProgressIndicator(
                            color: AppColors.ashGrey,
                          ),
                        ),
                        const SizedBox(
                          height: AppConstant.kDefaultPadding,
                        ),
                      ],
                    );
                  }
                  if (state is MovieByGenreError) {
                    return Center(
                      child: Text(
                        state.message,
                        style: AppTextStyle.mulishXSmall
                            .copyWith(color: Colors.black),
                      ),
                    );
                  }
                  return Container();
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
