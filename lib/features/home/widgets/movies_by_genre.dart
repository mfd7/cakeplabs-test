import 'package:cached_network_image/cached_network_image.dart';
import 'package:cakeplabs_test/util/app_colors.dart';
import 'package:cakeplabs_test/util/app_constant.dart';
import 'package:cakeplabs_test/util/app_flavor.dart';
import 'package:cakeplabs_test/util/app_text_style.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';

class MoviesByGenre extends StatelessWidget {
  const MoviesByGenre(
      {super.key,
      required this.title,
      required this.imageUrl,
      required this.releaseDate});

  final String title;
  final String imageUrl;
  final DateTime releaseDate;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        AspectRatio(
          aspectRatio: 1,
          child: Container(
            clipBehavior: Clip.antiAlias,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8),
            ),
            child: CachedNetworkImage(
              fit: BoxFit.cover,
              alignment: Alignment.topCenter,
              imageUrl: '${AppFlavor.instance.imgUrl}$imageUrl',
              progressIndicatorBuilder: (_, url, __) {
                return const Center(
                  child: CircularProgressIndicator(
                    color: AppColors.dawnPink,
                  ),
                );
              },
              errorWidget: (_, error, __) {
                return Center(
                  child: Text(
                    error,
                    style: AppTextStyle.mulishXSmall
                        .copyWith(color: AppColors.black),
                  ),
                );
              },
            ),
          ),
        ),
        const SizedBox(
          height: AppConstant.kDefaultPadding / 4 * 3,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            title,
            style: AppTextStyle.mulishMedium.copyWith(color: AppColors.black),
            overflow: TextOverflow.ellipsis,
          ),
        ),
        const SizedBox(
          height: AppConstant.kDefaultPadding / 4,
        ),
        Align(
          alignment: Alignment.centerLeft,
          child: Text(
            DateFormat('MMM dd, yyyy').format(releaseDate),
            style: AppTextStyle.mulishSmall.copyWith(color: AppColors.osloGrey),
            overflow: TextOverflow.ellipsis,
          ),
        ),
      ],
    );
  }
}
