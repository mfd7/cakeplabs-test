import 'dart:async';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:cakeplabs_test/core/domains/movie/entities/movie.dart';
import 'package:cakeplabs_test/features/home/widgets/dot_navigation.dart';
import 'package:cakeplabs_test/util/app_colors.dart';
import 'package:cakeplabs_test/util/app_constant.dart';
import 'package:cakeplabs_test/util/app_flavor.dart';
import 'package:cakeplabs_test/util/app_text_style.dart';
import 'package:flutter/material.dart';

class NowPlaying extends StatefulWidget {
  const NowPlaying({super.key, required this.movies});
  final List<Movie> movies;

  @override
  State<NowPlaying> createState() => _NowPlayingState();
}

class _NowPlayingState extends State<NowPlaying> {
  final pageController =
      PageController(initialPage: 1000, viewportFraction: 0.9);
  int pageNumber = 0;
  int autoPlayIndex = 1000;
  late final Timer slideTimer;

  Timer getSlideTimer() {
    return Timer.periodic(const Duration(seconds: 2), (timer) {
      var nextPage = autoPlayIndex + 1;
      pageController.animateToPage(
        nextPage,
        duration: const Duration(seconds: 1),
        curve: Curves.easeInOutCirc,
      );
    });
  }

  @override
  void initState() {
    super.initState();
    slideTimer = getSlideTimer();
  }

  @override
  void dispose() {
    pageController.dispose();
    slideTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 130,
          child: PageView.builder(
            padEnds: false,
            controller: pageController,
            physics: const BouncingScrollPhysics(),
            onPageChanged: (index) {
              setState(() {
                pageNumber = index % widget.movies.length;
                autoPlayIndex = index;
              });
            },
            itemBuilder: (context, index) {
              final adjustedIndex = index % widget.movies.length;

              return AnimatedBuilder(
                animation: pageController,
                builder: (ctx, child) {
                  return child!;
                },
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  margin: EdgeInsets.only(
                    left: AppConstant.kDefaultPadding,
                    right: adjustedIndex == widget.movies.length - 1
                        ? AppConstant.kDefaultPadding
                        : 0,
                  ),
                  width: 328,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: CachedNetworkImage(
                    fit: BoxFit.cover,
                    imageUrl:
                        '${AppFlavor.instance.imgUrl}${widget.movies[adjustedIndex].backdropUrl}',
                    progressIndicatorBuilder: (_, url, __) {
                      return const Center(
                        child: CircularProgressIndicator(
                          color: AppColors.dawnPink,
                        ),
                      );
                    },
                    errorWidget: (_, error, __) {
                      return Center(
                        child: Text(
                          error,
                          style: AppTextStyle.mulishXSmall
                              .copyWith(color: AppColors.black),
                        ),
                      );
                    },
                  ),
                ),
              );
            },
          ),
        ),
        const SizedBox(
          height: AppConstant.kDefaultPadding,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: List.generate(
            widget.movies.length,
            (index) => DotNavigation(
              isSelected: pageNumber == index,
            ),
          ),
        ),
      ],
    );
  }
}
