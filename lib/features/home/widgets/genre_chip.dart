import 'package:cakeplabs_test/util/app_colors.dart';
import 'package:cakeplabs_test/util/app_text_style.dart';
import 'package:flutter/material.dart';

class GenreChip extends StatelessWidget {
  const GenreChip({
    super.key,
    required this.title,
    required this.isSelected,
    required this.onTap,
  });

  final String title;
  final bool isSelected;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      shape: const CircleBorder(),
      child: InkWell(
        customBorder: const CircleBorder(),
        onTap: onTap,
        child: Container(
          width: 70,
          height: 70,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            border: Border.all(
                color: isSelected ? AppColors.smokeyGrey : AppColors.dawnPink,
                width: 2),
          ),
          child: Center(
            child: Text(
              title,
              style: AppTextStyle.mulishXSmall.copyWith(
                  color: AppColors.black, overflow: TextOverflow.ellipsis),
            ),
          ),
        ),
      ),
    );
  }
}
