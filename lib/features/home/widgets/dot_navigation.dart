import 'package:cakeplabs_test/util/app_colors.dart';
import 'package:flutter/material.dart';

class DotNavigation extends StatelessWidget {
  const DotNavigation({
    super.key,
    this.isSelected = false,
  });

  final bool isSelected;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(horizontal: 2.5),
      height: isSelected ? 10 : 8,
      width: isSelected ? 10 : 8,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: isSelected ? AppColors.black : AppColors.ashGrey,
      ),
    );
  }
}
