part of 'genre_list_bloc.dart';

@immutable
abstract class GenreListState extends Equatable {

  const GenreListState();

  @override
  List<Object?> get props => [];
}

class GenreListInitial extends GenreListState {}
class GenreListLoading extends GenreListState{}
class GenreListLoaded extends GenreListState{
  final List<Genre> genres;

  const GenreListLoaded(this.genres);

  @override
  List<Object> get props => [genres];
}
class GenreListError extends GenreListState{
  final String message;

  const GenreListError(this.message);

  @override
  List<Object> get props => [message];
}
