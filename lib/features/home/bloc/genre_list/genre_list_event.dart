part of 'genre_list_bloc.dart';

@immutable
abstract class GenreListEvent {}

class OnRetrieveGenreList extends GenreListEvent {}
