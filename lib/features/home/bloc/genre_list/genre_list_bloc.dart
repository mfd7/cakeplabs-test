import 'package:cakeplabs_test/core/domains/genre/entities/genre.dart';
import 'package:cakeplabs_test/core/domains/genre/usecases/retrieve_genre_list_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'genre_list_event.dart';
part 'genre_list_state.dart';

class GenreListBloc extends Bloc<GenreListEvent, GenreListState> {
  final RetrieveGenreListUsecase _retrieveGenreListUsecase;
  GenreListBloc(this._retrieveGenreListUsecase) : super(GenreListInitial()) {
    on<OnRetrieveGenreList>((event, emit) async {
      emit(GenreListLoading());
      final genres = await _retrieveGenreListUsecase.execute();
      genres.fold((failure) {
        emit(GenreListError(failure.message));
      }, (data) {
        emit(GenreListLoaded(data));
      });
    });
  }
}
