part of 'movie_by_genre_bloc.dart';

@immutable
abstract class MovieByGenreState extends Equatable {
  const MovieByGenreState();

  @override
  List<Object?> get props => [];
}

class MovieByGenreInitial extends MovieByGenreState {}

class MovieByGenreLoading extends MovieByGenreState {}

class MovieByGenreLoaded extends MovieByGenreState {
  final MovieByGenre movies;

  const MovieByGenreLoaded(this.movies);

  MovieByGenreLoaded copyWith({
    MovieByGenre? movies,
  }) {
    return MovieByGenreLoaded(movies ?? this.movies);
  }

  @override
  List<Object?> get props => [movies];
}

class MovieByGenreError extends MovieByGenreState {
  final String message;

  const MovieByGenreError(this.message);

  @override
  List<Object?> get props => [message];
}
