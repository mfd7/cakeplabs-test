import 'package:bloc_concurrency/bloc_concurrency.dart';
import 'package:cakeplabs_test/core/domains/movie/entities/movie_by_genre.dart';
import 'package:cakeplabs_test/core/domains/movie/usecases/retrieve_movie_by_genre_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'movie_by_genre_event.dart';
part 'movie_by_genre_state.dart';

class MovieByGenreBloc extends Bloc<MovieByGenreEvent, MovieByGenreState> {
  final RetrieveMovieByGenreUsecase _retrieveMovieByGenreUsecase;
  int page = 1;
  bool hasNext = true;

  MovieByGenreBloc(this._retrieveMovieByGenreUsecase)
      : super(MovieByGenreInitial()) {
    on<OnRetrieveMovieByGenre>((event, emit) async {
      if (!event.nextPage || !hasNext) {
        page = 1;
        emit(MovieByGenreLoading());
      } else {
        page++;
      }
      MovieByGenre mov = MovieByGenre();
      final movies = await _retrieveMovieByGenreUsecase.execute(
          genreId: event.genreId, page: page);
      movies.fold((failure) {
        emit(MovieByGenreError(failure.message));
      }, (data) {
        if (event.nextPage && page < data.totalPage) {
          mov = (state as MovieByGenreLoaded).movies.copyWith(
              movies: List.of((state as MovieByGenreLoaded).movies.movies)
                ..addAll(data.movies));
          if (page == data.totalPage) {
            hasNext = false;
          }
        } else {
          mov = data;
        }
        emit(MovieByGenreLoaded(mov));
      });
    }, transformer: droppable());
  }
}
