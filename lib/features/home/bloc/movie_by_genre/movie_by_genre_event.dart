part of 'movie_by_genre_bloc.dart';

@immutable
abstract class MovieByGenreEvent {}

class OnRetrieveMovieByGenre extends MovieByGenreEvent {
  final int? genreId;
  final bool nextPage;

  OnRetrieveMovieByGenre({this.genreId, this.nextPage = false});
}
