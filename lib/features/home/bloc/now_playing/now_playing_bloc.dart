import 'package:cakeplabs_test/core/domains/movie/entities/movie.dart';
import 'package:cakeplabs_test/core/domains/movie/usecases/retrieve_now_playing_usecase.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

part 'now_playing_event.dart';
part 'now_playing_state.dart';

class NowPlayingBloc extends Bloc<NowPlayingEvent, NowPlayingState> {
  final RetrieveNowPlayingUsecase _nowPlayingUsecase;

  NowPlayingBloc(this._nowPlayingUsecase) : super(NowPlayingInitial()) {
    on<OnRetrieveNowPlayingList>((event, emit) async {
      emit(NowPlayingLoading());
      final nowPlaying = await _nowPlayingUsecase.execute();
      nowPlaying.fold((failure) {
        emit(NowPlayingError(failure.message));
      }, (data) {
        emit(NowPlayingLoaded(data));
      });
    });
  }
}
